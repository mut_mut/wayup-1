//1

let city = 'lviv';
let country = 'ukraine';
let population = 100000;
let footballStad = false;

//2

const width = 70;
const height = 40;
let area = width * height;

//3

const time = 2;
const speedOfFirst = 95;
const speedOfSecond = 114;

let kmOfFirst = speedOfFirst * time;
let kmOfSecond = speedOfSecond * time;

let kmBetweenTown = kmOfFirst + kmOfSecond;

//4

const randomNumber = Math.floor(Math.random() * 100);

console.log('---' + randomNumber + '---');

if (randomNumber < 20) {
    console.log("randomNumber меньше 20");
} else if (randomNumber > 50) {
    console.log("randomNumber больше 50");
} else {
    console.log("randomNumber больше 20, и меньше 50");
}

//5

switch (true) {
    case (randomNumber < 20) :
        console.log("randomNumber меньше 20");
        break;
    case (randomNumber> 50) :
        console.log("randomNumber больше 50");
        break;
    default:
        console.log("randomNumber больше 20, и меньше 50");
}


